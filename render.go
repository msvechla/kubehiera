package main

type Render struct {
	Environment map[string]string `short:"e" long:"environment" description:"key:value pairs"`
	Output      string            `short:"o" long:"output" description:"output path of the rendered file.If not specified it will be rendered to stdout."`
	Inplace     bool              `short:"i" long:"inplace" description:"if specified, the input file will be overwritten with the rendered version."`
	RenderOpts  struct {
		TemplateFile string
	} `positional-args:"yes" required:"yes" command:"render"`
}

var render Render

func (r *Render) Execute(args []string) error {
	m := Merger{HieraPath: options.HieraPath, MergerIdentity: r.Environment, Input: r.RenderOpts.TemplateFile, Output: r.Output}
	m.init()
	m.run()
	return nil
}

func init() {
	parser.AddCommand("render", "renders a template", "main command, renders a template file to an output dir", &render)
}
