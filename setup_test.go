package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"testing"
)

func TestSetup(t *testing.T) {

	dir, err := ioutil.TempDir("", "cryptotest")
	if err != nil {
		log.Fatal(err)
	}

	defer os.RemoveAll(dir) // clean up
	c := PKCS7Encryption{OutputDir: fmt.Sprintf("%s/", dir)}
	c.setup()

	testCertCMD := exec.Command("openssl", "x509", "-in", filepath.Join(dir, "cert.pem"))
	_, err = testCertCMD.Output()
	if err != nil {
		t.Fatalf("openssl command failed to verify cert.pem: %s", err)
	}

	testKeyCMD := exec.Command("openssl", "rsa", "-in", filepath.Join(dir, "key.pem"))
	_, err = testKeyCMD.Output()
	if err != nil {
		t.Fatalf("openssl command failed to verify key.pem: %s", err)
	}

}
