# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.1] - 2017-11-29
### Added
- Automated builds for darwin, linux and windows on amd64
- Updated README to point to the latest releases

## [1.0.0] - 2017-11-29
### Added
- Initial version release
- Merging capability
- Render capability
- ENV CryptoBackend
- PKCS7 CryptoBackend
- Examples
- Tests
- CI Pipeline
- README
- CHANGELOG
- ...
